import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import {fetchPlanets} from "../../store/actions/planets";
import FormArea from "../../components/FormArea/FormArea";
import {createArea} from "../../store/actions/areas";


class NewArea extends Component {
    componentDidMount() {
        this.props.onFetchPlanets();
    }
    createArea = areaData => {
        this.props.onAreaCreated(areaData).then(() => {
        });
    };

    render() {
        return (
            <Fragment>
                <PageHeader>New area</PageHeader>
                <FormArea onSubmit={this.createArea} />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAreaCreated: areaData => {
            return dispatch(createArea(areaData))
        },
        onFetchPlanets: () => dispatch(fetchPlanets())
    }
};

export default connect(null, mapDispatchToProps)(NewArea);