import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import {Link} from "react-router-dom";
import {deletePlanet, fetchPlanets} from "../../store/actions/planets";
import PlanetList from "../../components/PlanetList/PlanetList";


class Planet extends Component {

    state = {
        deletedPlanet: null
    };


    componentDidMount() {
        this.props.onFetchPlanets();
    }
    componentDidUpdate(prevProps, prevState) {
        if(prevState.deletedPlanet !== this.state.deletedPlanet) {
            this.props.onFetchPlanets();
        }
    }

    onDeletePlanet = async (id) => {
        await this.props.deletePlanet(id);
        this.setState(() => {
            return {deletedPlanet: id}
        });

    };


    render() {
        return (
            <Fragment>
                <PageHeader>
                    Planets
                    <Link to="/planets/new">
                    </Link>
                </PageHeader>

                {this.props.planets.map(planet => (
                    <PlanetList
                        key={planet._id}
                        id={planet._id}
                        name={planet.name}
                        image={planet.image}
                        type={planet.type}
                        user={this.props.user}
                        click={this.props.user ? () => this.onDeletePlanet(planet._id) : null}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        planets: state.planets.planets,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchPlanets: () => dispatch(fetchPlanets()),
        deletePlanet: (id) => dispatch(deletePlanet(id))
       
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Planet);