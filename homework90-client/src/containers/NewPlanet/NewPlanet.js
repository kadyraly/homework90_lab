import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import FormPlanet from "../../components/FormPlanet/FormPlanet";
import {createPlanet} from "../../store/actions/planets";


class NewPlanet extends Component {

    createPlanet = planetData => {
        this.props.onPlanetCreated(planetData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <PageHeader>New planet</PageHeader>
                <FormPlanet onSubmit={this.createPlanet} />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPlanetCreated: planetData => {
            return dispatch(createPlanet(planetData))
        }

    }
};

export default connect(null, mapDispatchToProps)(NewPlanet);