import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import { PageHeader} from "react-bootstrap";
import AreaList from "../../components/AreaList/AreaList";
import {deleteArea, fetchAreas, sellArea} from "../../store/actions/areas";


class Area extends Component {
    state = {
        deletedArea: null
    };

    componentDidMount() {
        this.props.onFetchAreas(this.props.match.params.id);
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.deletedArea !== this.state.deletedArea) {
            this.props.onFetchAreas(this.props.match.params.id);
        }
    }

    onDeleteArea = async (id) => {
        await this.props.deleteArea(id);
        this.setState(() => {
            return {deletedArea: id}
        });

    };

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Areas
                </PageHeader>

                {this.props.areas.map(area => (
                    <AreaList
                        key={area._id}
                        id={area._id}
                        quantity={area.quantity}
                        price={area.price}
                        planet={area.planet.name}
                        sold={area.sold}
                        description={area.description}
                        user={this.props.user}
                        click={this.props.user ? () => this.onDeleteArea(area._id) : null}
                        clickSell={this.props.user ? () => this.props.sellArea(area._id,this.props.user.token ) : null}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        areas: state.areas.areas,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchAreas: (id) => dispatch(fetchAreas(id)),
        sellArea: (id) => dispatch(sellArea(id)),
        deleteArea: (id) => dispatch(deleteArea(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Area);