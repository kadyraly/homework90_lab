import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import { PageHeader} from "react-bootstrap";
import SoldAreaList from "../../components/SoldAreaList/SoldAreaList";
import {fetchSell} from "../../store/actions/soldAreas";

class SoldAreas extends Component {
    componentDidMount() {
        if(this.props.user) {
            this.props.onFetchSell(this.props.user.token);
        } else {
            this.props.history.push('/login');
        }
    }

    render() {
        if(!this.props.sells) {
            return <div>Loading</div>
        }
        return (
            <Fragment>
                <PageHeader>
                    Sold areas
                </PageHeader>

                {this.props.sells.map(sell => {
                    if(sell.planet !== null) {
                        return (
                            <SoldAreaList
                                key={sell._id}
                                id={sell._id}
                                planet={sell.planet.name}
                                quantity={sell.quantity}
                                price={sell.price}
                                datetime={sell.datetime}
                            />
                        )
                    }

                })}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        sells: state.sells.sells,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchSell: (id) => dispatch(fetchSell(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SoldAreas);