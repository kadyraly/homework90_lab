import React from 'react';
import {Redirect, Route, Switch, withRouter} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {connect} from "react-redux";
import SoldAreas from "./containers/SoldAreas/SoldAreas";
import NewArea from "./containers/NewArea/NewArea";
import Planet from "./containers/Planet/Planet";
import Area from "./containers/Area/Area";
import NewPlanet from "./containers/NewPlanet/NewPlanet";

const ProtectedRoute = ({isAllowed, ...props}) => (
    isAllowed ? <Route {...props} /> : <Redirect to ='/login' />
);

const Routes = ({user}) => (
    <Switch>
        <Route path="/" exact component={Planet} />
        <Route path="/areas" exact component={Area} />
        <ProtectedRoute isAllowed={user && user.role === 'admin'}
                        path="/planets/new"
                        exact component={NewPlanet}
        />
        <ProtectedRoute isAllowed={user && user.role === 'admin'}
                        path="/areas/new"
                        exact component={NewArea}
        />
        <Route path="/planets/:id" exact component={Area} />
        <Route path="/sold_areas" exact component={SoldAreas} />


        <Route path="/register" exact component={Register} />
        <Route path="/login" exact component={Login} />
    </Switch>

);

const mapStateToProps = state => ({
    user: state.users.user
});



export default withRouter(connect(mapStateToProps)(Routes));

