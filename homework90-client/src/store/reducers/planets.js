import { FETCH_PLANETS_SUCCESS} from "../actions/actionTypes";


const initialState = {
    planets: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_PLANETS_SUCCESS:
            return {...state, planets: action.planets};
        default:
            return state;
    }
};

export default reducer;