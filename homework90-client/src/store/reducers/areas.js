import {FETCH_AREAS_SUCCESS} from "../actions/actionTypes";


const initialState = {
    areas: []

};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_AREAS_SUCCESS:
            return {...state, areas: action.areas};

        default:
            return state;
    }
};

export default reducer;