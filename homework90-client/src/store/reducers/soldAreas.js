import {FETCH_SELL_SUCCESS} from "../actions/actionTypes";


const initialState = {
    sells: null
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_SELL_SUCCESS:
            return {...state, sells: action.sells};
        default:
            return state;
    }
};

export default reducer;