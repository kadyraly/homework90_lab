import axios from '../../axios-api';
import {CREATE_PLANET_SUCCESS, DELETE_PLANET_SUCCESS, FETCH_PLANETS_SUCCESS} from "./actionTypes";



export const fetchPlanetsSuccess = planets => {
    return {type: FETCH_PLANETS_SUCCESS, planets};
};

export const fetchPlanets = () => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        axios.get(user && user.role === 'admin' ? '/planets/admin' : '/planets').then(
            response => dispatch(fetchPlanetsSuccess(response.data))
        );
    }
};

export const createPlanetSuccess = () => {
    return {type: CREATE_PLANET_SUCCESS};
};

export const createPlanet = planetData => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.post('/planets', planetData, {headers}).then(
            response => dispatch(createPlanetSuccess())
        );
    };
};

export const deletePlanetSuccess = () => {
    return {type: DELETE_PLANET_SUCCESS};
};

export const deletePlanet = (id) => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.delete('/planets/sold/' + id, {headers}).then(
            response => dispatch(deletePlanetSuccess())
        );
    };
};
