import axios from '../../axios-api';
import {CREATE_SELL_SUCCESS, FETCH_SELL_SUCCESS} from "./actionTypes";

export const createSellSuccess = sells => {
    return {type: CREATE_SELL_SUCCESS, sells};
};
export const fetchSellSuccess = sells => {
    return {type: FETCH_SELL_SUCCESS, sells};
};

export const fetchSell = (token) => {
    return dispatch => {
        axios.get('/sold_areas', {headers: {"Token": token}}).then(
            response => {
                dispatch(fetchSellSuccess(response.data))
            }
        );
    }
};

export const createSell = (sellData, token) => {
    return dispatch => {
        axios.post('/sold_areas', sellData, {headers: {"Token": token}}).then(
            response => dispatch(createSellSuccess(response.data))

        );
    }

};
