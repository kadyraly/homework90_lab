import axios from '../../axios-api';

import {push} from "react-router-redux";
import {
    CREATE_AREAS_SUCCESS, DELETE_AREAS_SUCCESS, FETCH_AREAS_SUCCESS,
    SELL_AREAS_SUCCESS
} from "./actionTypes";


export const fetchAreasSuccess = areas => {
return {type: FETCH_AREAS_SUCCESS, areas};
};

export const fetchAreas = (id) => {
    return (dispatch, getState) => {
        const user = getState().users.user;
            axios.get(user && user.role === 'admin' ? '/areas/admin?planets=' + id : '/areas?planets=' + id).then(
                response => dispatch(fetchAreasSuccess(response.data))
            );

        }

};

export const createAreaSuccess = () => {
    return {type: CREATE_AREAS_SUCCESS};
};

export const createArea = areaData => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.post('/areas', areaData, {headers}).then(
            response => {
                dispatch(push('/planets/' + response.data.planet));
                dispatch(createAreaSuccess())
            }
        );
    };
};

export const sellAreaSuccess = (soldArea) => {
    return {type: SELL_AREAS_SUCCESS, soldArea};
};

export const sellArea = (id) => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };

        return axios.put('/areas/sold/' + id, {headers}).then(
            response => {
                dispatch(fetchAreas(response.data.planet))
            }
        );
    };
};
export const deleteAreaSuccess = () => {
    return {type: DELETE_AREAS_SUCCESS};
};

export const deleteArea = (id) => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.delete('/areas/sold/' + id, {headers}).then(
            response => dispatch(deleteAreaSuccess())
        );
    };
};
