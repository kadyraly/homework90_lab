import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../UI/FormElement/FormElement";
import {connect} from "react-redux";
import {fetchPlanets} from "../../store/actions/planets";

class FormArea extends Component {
    state = {
        quantity: '',
        planet: '',
        price: '',
        description: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.onSubmit(this.state);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        const planet = this.props.planets.map(planet => {
            return {
                id: planet._id,
                name: planet.name
            }
        });
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName='quantity'
                    title="Quantity of area"
                    type='number'
                    value={this.state.quantity}
                    changeHandler={this.inputChangeHandler}
                    required
                />
                <FormElement
                    propertyName="planet"
                    title="Planet of area"
                    type='select'
                    options={planet}
                    value={this.state.planet}
                    changeHandler={this.inputChangeHandler}
                    required
                />
                <FormElement
                    propertyName="description"
                    title="Description of area"
                    type="textarea"
                    value={this.state.description}
                    changeHandler={this.inputChangeHandler}
                    required
                />
                <FormElement
                    propertyName="price"
                    title="Price of area"
                    type="number"
                    value={this.state.price}
                    changeHandler={this.inputChangeHandler}
                    required
                />

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => {
    return {
        planets: state.planets.planets
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchPlanets: (id) => dispatch(fetchPlanets(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps) (FormArea);