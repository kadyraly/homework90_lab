import React, {Fragment} from 'react';
import {Button, Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

import notFound from '../../assets/image/not-found.png';

const PlanetList= props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + '/uploads/' + props.image;
    }

    return (
        <Panel>
            <Panel.Body>
                <Image
                    style={{width: '100px', marginRight: '10px'}}
                    src={image}
                    thumbnail
                />
                <Link to={'/planets/' + props.id}>
                    {props.name}
                </Link>
                {props.user && props.user.role === 'admin' ?
                    <Fragment>
                        <Button className="pull-right" onClick={props.click} bsStyle="primary" >Delete</Button>
                    </Fragment> : null
                }
            </Panel.Body>

        </Panel>
    );
};

PlanetList.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    image: PropTypes.string
};

export default PlanetList;