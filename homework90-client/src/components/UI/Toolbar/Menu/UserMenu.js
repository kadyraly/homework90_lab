import React, {Fragment} from "react";
import {MenuItem, Nav, NavDropdown, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const UserName = ({user, logout}) => {
    const navTitle = (
        <Fragment>
            hello, <b>{user.username}</b>!
        </Fragment>
    );
    return (
        <Nav pullRight>
            <NavDropdown title={navTitle} id="user-menu">
                <LinkContainer to="/sold_areas/" exact>
                    <NavItem>Sold Areas</NavItem>
                </LinkContainer>
                {user && user.role === 'admin' ?
                    <Fragment>
                        <LinkContainer to="/planets/new" exact>
                            <NavItem>Add new planet</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/areas/new" exact>
                            <NavItem>Add new area</NavItem>
                        </LinkContainer>
                    </Fragment> : null}

                <MenuItem divider/>
                <MenuItem onClick={logout}>Logout</MenuItem>
            </NavDropdown>
        </Nav>
    )

};

export  default  UserName;