import React, {Fragment} from 'react';
import {Button,  Panel} from "react-bootstrap";
import PropTypes from 'prop-types';


const AreaList = props => {


    return (
        <Panel>
            Planet name: <b> {props.planet} </b>
            <Panel.Body>
                Description: {props.description} <br/> Quantity: <b> {props.quantity} </b> <br/>
                Price: <strong style={{marginLeft: '10px'}}> {props.price} KGS  </strong>
                {props.user && props.user.role === 'admin'  ?
                    < Fragment >
                        {!props.sold ? <Button className="pull-right" onClick={props.clickSell} bsStyle="primary" >Sell</Button> : null}
                        <Button className="pull-right" onClick={props.click} bsStyle="primary" >Delete</Button>
                    </Fragment> : null
                }
            </Panel.Body>
        </Panel>
    );
};

AreaList.propTypes = {
    id: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    description: PropTypes.string.isRequired,
    planet: PropTypes.string.isRequired
};

export default AreaList;