import React from 'react';
import {Panel} from "react-bootstrap";
import PropTypes from 'prop-types';


const SoldAreaList = props => {


    return (
        <Panel>
            <Panel.Heading>Planet: <b>{props.planet} </b></Panel.Heading>
            <Panel.Body> Quantity of area: <b> {props.quantity} </b> <br/>
                        Total price: <strong>{props.quantity * props.price} KG com</strong></Panel.Body>
            <Panel.Footer>{props.datetime}</Panel.Footer>
        </Panel>
    );
};

SoldAreaList.propTypes = {
    id: PropTypes.string.isRequired,
    planet: PropTypes.string.isRequired,
    quantity: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    datetime: PropTypes.string

};

export default SoldAreaList;