import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux';
import {ConnectedRouter} from "react-router-redux";
import store, {history} from "./store/configureStore";
import axios from './axios-api';
import {logoutExpiredUser} from "./store/actions/users";

axios.interceptors.request.use(config => {
    try {
        config.headers['Token'] = store.getState().users.token;
    } catch (e) {

    }
    return config;
});

axios.interceptors.response.use(response => response, error => {
    if ( error.response.status === 401) {
        store.dispatch(logoutExpiredUser());
    }
});


const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();