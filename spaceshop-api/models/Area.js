const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AreaSchema = new Schema({
  quantity: {
    type: Number,
    required: true
  },
  planet: {
    type: Schema.Types.ObjectId,
    ref: 'Planet',
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  sold: {
      type: Boolean,
      default: false,
      required: true
  },
  description: {
    type: String,
    required: true
  },
  datetime: String
});

const Area = mongoose.model('Area', AreaSchema);

module.exports = Area;