const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const SoldAreasSchema = new Schema({
    planet: {
        type: Schema.Types.ObjectId,
        ref: 'Planet',
        required: true
    },
    area: {
        type: Schema.Types.ObjectId,
        ref: 'Area',
        required: true
    },
    datetime: {
        type: String
    }

});

const SoldAreas = mongoose.model('SoldAreas', SoldAreasSchema);
module.exports = SoldAreas;