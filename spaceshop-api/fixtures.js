const mongoose = require('mongoose');
const config = require('./config');
const Planet = require('./models/Planet');
const Area = require('./models/Area');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;
const collections = ['planets', 'areas', 'users'];

db.once('open', async () => {

    collections.forEach(async () => {
        try {
            await db.dropCollection('planets');
            await db.dropCollection('areas');
            await db.dropCollection('users');
        } catch (e) {
            console.log(`Collection did not exist in DB`);
        }
    });

    const [Mars, Moon] = await Planet.create({
        name: 'Mars',
        type: 'earth type'
    }, {
        name: 'Moon',
        type: 'like earth'
    });

    await Area.create({
        quantity: '2',
        planet: Mars._id,
        price: 3000,
        description: 'It is good place for living'
    }, {
        quantity: '3',
        planet: Moon._id,
        price: 3000,
        description: 'It is to live very dangerous'
    });
    await User.create({
        username: 'user',
        password: '123',
        role: 'user'
    }, {
        username: 'admin',
        password: 'admin123',
        role: 'admin'
    });

    db.close();

});
