const express = require('express');
const Area = require('../models/Area');
const auth = require('../middleware/auth');

const router = express.Router();

const createRouter = (db) => {

    router.get('/admin', (req, res) => {
        if (req.query.planets) {
            Area.find({planet: req.query.planets}).populate('planet')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        } else {
            Area.find().populate('planet')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });

    router.get('/', (req, res) => {
        if(req.query.planets) {
            Area.find({planet: req.query.planets}).populate('planet')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        } else {
            Area.find({sold: true}).populate('planet')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });


    router.post('/', auth, (req, res) => {
        const areaData = req.body;
        const area = new Area(areaData);

        area.save().then(area => {
            res.send(area)
        })
    });

    router.put('/sold/:id', async (req, res) => {
        let area = await Area.findOne({_id: req.params.id});
        area.sold = true;
        area.datetime = new Date();

        await area.save();

        res.send(area);
    });

    router.delete('/sold/:id', async (req, res) => {

        let area = await Area.findByIdAndRemove({_id: req.params.id});

        res.send(area);
    });


  return router;

};

module.exports = createRouter;