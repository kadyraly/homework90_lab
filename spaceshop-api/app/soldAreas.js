const express = require('express');

const User = require('../models/User');
const Areas = require('../models/Area');
const router = express.Router();


const createRouter = () => {
    router.get('/', async (req, res) => {

        Areas.find({sold: true}).populate('planet')
            .then(results => {
                res.send(results)
            })
            .catch(() => res.sendStatus(500));
    });


    return router;
};

module.exports = createRouter;