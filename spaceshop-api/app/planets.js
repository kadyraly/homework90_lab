const express = require('express');
const Planet = require('../models/Planet');
const nanoid = require('nanoid');
const multer = require('multer');
const auth = require('../middleware/auth');
const config = require("../config");
const path = require('path');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {

        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = (db) => {
  // Planet index
  router.get('/', (req, res) => {
    Planet.find()
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  router.get('/admin', (req, res) => {
      Planet.find()
          .then(results => res.send(results))
          .catch(() => res.sendStatus(500));
  });

  // Planet get by ID
  // router.get('/:id', (req, res) => {
  //   const id = req.params.id;
  //   db.collection('planets')
  //     .findOne({_id: new ObjectId(req.params.id)})
  //     .then(result => {
  //       if (result) res.send(result);
  //       else res.sendStatus(404);
  //     })
  //     .catch(() => res.sendStatus(500));
  // });

    router.post('/', [auth, upload.single('image')], (req, res) => {

        const planetData = req.body;

        if (req.file) {
            planetData.image = req.file.filename;
        } else {
            planetData.image = null;
        }

        const planet = new Planet(planetData);
        planet.save()
            .then(result => res.send(result))
            .catch(() => res.sendStatus(500));
    });

    router.delete('/sold/:id', async (req, res) => {

        let planet = await Planet.findByIdAndRemove({_id: req.params.id});

        res.send(planet);
    });



    return router;
};

module.exports = createRouter;